import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {

  private loggedIn = false;

  constructor(private router: Router) { }

  isAuthenticated() {
   return this.loggedIn; 
  }

  login() {
    this.loggedIn = true;
    this.router.navigate([''], { replaceUrl: true })
  }
  
  logout() {
    this.loggedIn = false;
    this.router.navigate(['/login'], { replaceUrl: true })
  }
}
